package LinearAutomation;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CerotidWebsiteLinearExample {
	static WebDriver driver;

	public static void main(String[] args) {
		//Step 1: Invoke Browser got to Cerotid Website
		invokeBrowser();
		
		//Step 2: Fill form
		fillForm();
		
		//Step 3: Validate Success Message

	}

	private static void fillForm() {
		// Select Course
		//Creating WebElement obj finding location of the element
		WebElement course = driver.findElement(By.xpath("//select[@id='classType']"));
		//creating select obj and passing the element
		Select chooseCourse = new Select(course);
		//creating a string variable with course name
		String courseQAAutomation = "QA Automation";
		//selecting the course by visible text
		chooseCourse.selectByVisibleText(courseQAAutomation);
		
		//Select Session
		WebElement session = driver.findElement(By.xpath("//select[@id='sessionType']"));
		Select chooseSession = new Select(session);
		String sessionType = "Upcoming Session";
		chooseSession.selectByVisibleText(sessionType);
		
		//Type Fullname
		WebElement fullName = driver.findElement(By.xpath("//input[@id='name']"));
		fullName.sendKeys("Shila Bennett");
		
		//Type Address
		WebElement address = driver.findElement(By.xpath("//input[@id='address']"));
		address.sendKeys("Oram Circle");
		
		//Type City
		WebElement city = driver.findElement(By.xpath("//input[@id='city']"));
		city.sendKeys("Ogden");
		
		//Select State
		WebElement state = driver.findElement(By.xpath("//select[@id='state']"));
		Select chooseState = new Select(state);
		String stateUtah = "UT";
		chooseState.selectByVisibleText(stateUtah);
		
		//Type Zipcode
		WebElement zipcode = driver.findElement(By.xpath("//input[@id='zip']"));
		zipcode.sendKeys("84403");
		
		//Type Email
		WebElement email = driver.findElement(By.xpath("//input[@id='email']"));
		email.sendKeys("sangharshila@gmail.com");
		
		//Type Phone
		WebElement phone = driver.findElement(By.xpath("//input[@id='phone']"));
		phone.sendKeys("0221126479");
		
		//Select visa status
		WebElement visaStatus = driver.findElement(By.xpath("//select[@id='visaStatus']"));
		Select chooseStatus = new Select(visaStatus);
		String otherStatus = "Other";
		chooseStatus.selectByVisibleText(otherStatus);
		
		//Select source
		WebElement source = driver.findElement(By.xpath("//select[@id='mediaSource']"));
		Select chooseSource = new Select(source);
		String sourceFriend = "Friends/Family";
		chooseSource.selectByVisibleText(sourceFriend);
		
		//Select relocate
		WebElement relocate = driver.findElement(By.xpath("//input[@id='relocate'][2]"));
		relocate.click();
		
		//Type education background
		WebElement education = driver.findElement(By.xpath("//textarea[@id='eduDetails']"));
		education.sendKeys("M.Pharm");
	}

	private static void invokeBrowser() {
		// Set the System path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		// Creating new chromeDriver obj
		driver = new ChromeDriver();
		//Navigate to cerotid website
		driver.navigate().to("http://www.cerotid.com");
		
		
		System.out.println(driver.getTitle() + "-----------------was launched");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		
		
	}

}
