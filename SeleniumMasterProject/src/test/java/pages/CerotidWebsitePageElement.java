package pages;
//Purpose of this class is to return webelements

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CerotidWebsitePageElement {
	// Global Variable
	static WebElement element;

	// Course Element
	public static WebElement selectCourse(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='classType']"));
		return element;
	}
	//Session element
	public static WebElement chooseSession(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='sessionType']"));
		return element;
	}
	//Name element
	public static WebElement enterName(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@data-validation-required-message='Please enter your name.'][1]"));
		return element;
	}
	//Address element
	public static WebElement inputAddress(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='address']"));
		return element;
	}
	//City element
	public static WebElement enterCity(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='city']"));
		return element;
	}
	//State element
	public static WebElement selectState(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='state']"));
		return element;
	}
	//Zipcode element
	public static WebElement typeZipcode(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='zip']"));
	    return element;
	}
	//Email element
	public static WebElement inputEmail(WebDriver driver) {
		element= driver.findElement(By.xpath("//input[@id='email']"));
		return element;
	}
	//Phone element
	public static WebElement enterPhone(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='phone']"));
		return element;
	}
	//Visa Status element
	public static WebElement selectStatus(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='visaStatus']"));
		return element;
	}
	//Source element
	public static WebElement selectSource(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='mediaSource']"));
		return element;
	}
	//Relocation element
	public static WebElement selectRelocate(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='relocate'][2]"));
		return element;
	}
	//Education element
	public static WebElement inputEducation(WebDriver driver) {
		element = driver.findElement(By.xpath("//textarea[@id='eduDetails']"));
		return element;
	}

}
