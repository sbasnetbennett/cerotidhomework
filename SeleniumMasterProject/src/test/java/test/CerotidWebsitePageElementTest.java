package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import pages.CerotidWebsitePageElement;

//Created to test the page objects in CerotidWebsite
public class CerotidWebsitePageElementTest {
	
	static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		invokeBrowserCerotidPage();
		fillForm();
		
	}
	//Step 1:
	public static void invokeBrowserCerotidPage() throws InterruptedException {
		//Set the system path to point to the chromedriver.exe file
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		
		//Utilizing the driver global variable and creating a new driver
		driver = new ChromeDriver();
		
		//Navigate to Cerotid Website
		driver.navigate().to("http://www.cerotid.com");
		//Maximize the window
		driver.manage().window().maximize();
		//Wait example
		TimeUnit.SECONDS.sleep(4);
		//Refresh the screen
		driver.navigate().refresh();
		
		//Adding some condition
		String titleName = "Cerotid";
		if(driver.getTitle().contains(titleName)) {
			//Prints the title
			System.out.println(driver.getTitle() + "...........was launched");
		}else {
			System.out.println("Expected title " + titleName +" was not seen");
			System.out.println("Test Fail------ Ending Test");
			driver.quit();
			driver.close();
		}
		
	}
	
	//Step 2:
	public static void fillForm() {
		Select chooseCourse = new Select(CerotidWebsitePageElement.selectCourse(driver));
		chooseCourse.selectByVisibleText("QA Automation");
		
		Select choosesession = new Select(CerotidWebsitePageElement.chooseSession(driver));
		choosesession.selectByVisibleText("Upcoming Session");
		//Entering name
		CerotidWebsitePageElement.enterName(driver).sendKeys("John Smith");
		//Entering address
		CerotidWebsitePageElement.inputAddress(driver).sendKeys("Dan Drive");
		CerotidWebsitePageElement.enterCity(driver).sendKeys("Ogden");
		
		Select chooseState = new Select(CerotidWebsitePageElement.selectState(driver));
		chooseState.selectByVisibleText("UT");
		
		CerotidWebsitePageElement.typeZipcode(driver).sendKeys("84403");
		CerotidWebsitePageElement.inputEmail(driver).sendKeys("abcd@hotmail.com");
		CerotidWebsitePageElement.enterPhone(driver).sendKeys("9841415958");
		
		Select chooseStatus = new Select(CerotidWebsitePageElement.selectStatus(driver));
		chooseStatus.selectByVisibleText("Other");
		
		Select chooseSource = new Select(CerotidWebsitePageElement.selectSource(driver));
		chooseSource.selectByVisibleText("Friends/Family");
		
		//Clicking relocation option
		CerotidWebsitePageElement.selectRelocate(driver).click();
		
		CerotidWebsitePageElement.inputEducation(driver).sendKeys("MBA");
		
	}
	
	
	//Step 3:
	

}
