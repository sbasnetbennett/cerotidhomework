package test;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleTest {

	public static void main(String[] args) {
		//Creating webdriver obj
		//Setting the system path to utilize the chrome driver
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		
		//Creating Webdriver object to drive my browser
		WebDriver driver = new ChromeDriver();
		//Invoking the web browser
		driver.get("http://www.google.com");
		
		//Maximize the browser
		driver.manage().window().maximize();
		
		//Created a webElement obj for the txtBoxSearch
		WebElement txtBoxSearch = driver.findElement(By.xpath("//input[@name='q']"));
		txtBoxSearch.sendKeys("what is selenium");
		
		//Created a webElement obj for the searchBtn
		WebElement searchBtn = driver.findElement(By.xpath("//input[@name='btnK']"));
		searchBtn.sendKeys(Keys.RETURN);
		

	}

}
